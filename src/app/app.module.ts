import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { SharedModule } from '@app/shared';
import { CoreModule } from '@app/core';

import { SettingsModule } from './settings';
import { StaticModule } from './static';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { KafkaconnectModule } from './kafkaconnect/kafkaconnect.module';
import { MatTableModule, MatPaginatorModule, MatDialogModule, MatIconModule } from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { OverlayContainer } from '@angular/cdk/overlay';
import { KafkaconnectService } from './kafkaconnect/kafkaconnect.service';


@NgModule({
  imports: [
    // angular
    BrowserAnimationsModule,
    BrowserModule,

    // core & shared
    CoreModule,
    SharedModule,

    // features
    StaticModule,
    SettingsModule,

    // app
    AppRoutingModule,

    // kafka connect
    KafkaconnectModule,

    MatTableModule,

    HttpClientModule,

    MatPaginatorModule,
    MatDialogModule,
    MatIconModule
  ],
  declarations: [AppComponent],
  providers: [KafkaconnectService],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(overlayContainer: OverlayContainer) {
    overlayContainer.getContainerElement()
                    .classList.add('black-theme');
}
}
