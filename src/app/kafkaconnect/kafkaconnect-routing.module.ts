import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SourceComponent } from './source/source.component';
import { KafkaconnectComponent } from './kafkaconnect/kafkaconnect.component';

const routes: Routes = [
  {
    path: '',
    component: KafkaconnectComponent,
    children: [
      {
        path: '',
        redirectTo: 'source',
        pathMatch: 'full'
      },
      {
        path: 'source',
        component: SourceComponent,
        data: {
          title: 'SOURCE'
        }
      },
      {
        path: 'sink',
        component: SourceComponent,
        data: {
          title: 'SINK'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class KafkaconnectRoutingModule {}
