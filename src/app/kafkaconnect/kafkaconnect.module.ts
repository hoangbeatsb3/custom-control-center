import { NgModule } from '@angular/core';
import { SharedModule } from '@app/shared';

import { KafkaconnectRoutingModule } from './kafkaconnect-routing.module';
import { KafkaconnectComponent } from './kafkaconnect/kafkaconnect.component';
import { SourceComponent } from './source/source.component';
import { MatStepperModule } from '@angular/material/stepper';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatTableModule, MatPaginatorModule, MatIconModule } from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { KafkaconnectService } from './kafkaconnect.service';
import { SourceService } from './source/source.service';

@NgModule({
  imports: [
    SharedModule,
    KafkaconnectRoutingModule,
    MatStepperModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableModule,
    HttpClientModule,
    MatPaginatorModule,
    MatIconModule
  ],
  declarations: [
    KafkaconnectComponent,
    SourceComponent,
  ],
  providers: [
    SourceComponent,
    KafkaconnectService,
    SourceService
  ]
})
export class KafkaconnectModule {
  constructor() {}
}

