import { Component, OnInit } from '@angular/core';
import { Input, HostListener } from '@angular/core';
import { NgModule } from '@angular/core';
import { routeAnimations } from '@app/core';
import { SourceComponent } from '../source/source.component';
import { KafkaconnectService } from '../kafkaconnect.service';

@Component({
  selector: 'anms-kafkaconnect',
  templateUrl: './kafkaconnect.component.html',
  styleUrls: ['./kafkaconnect.component.scss'],
  animations: [routeAnimations]
})

@NgModule({
  imports: [

  ],
  declarations: [
    SourceComponent
  ],
  providers: [
    SourceComponent,
    KafkaconnectService
  ]
})

export class KafkaconnectComponent implements OnInit {
  examples = [
    { link: 'source', label: 'Source' },
    { link: 'sink', label: 'Sink' }
  ];

  constructor(private _kafkaconnectService: KafkaconnectService) {

  }

  ngOnInit() {}

  setShowData() {
    this._kafkaconnectService.setShow();
  }

}
