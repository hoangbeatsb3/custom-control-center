import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { routeAnimations } from '@app/core';
import { MatDialog } from '@angular/material';
import { KafkaconnectService } from '../kafkaconnect.service';
import { SourceService } from './source.service';
import { Test } from '../model/test.model';

export interface DialogData {
  connectorType: string;
  connectorName: string;
}

@Component({
  selector: 'anms-source',
  templateUrl: './source.component.html',
  styleUrls: ['./source.component.scss'],
  animations: [routeAnimations]
})

export class SourceComponent implements OnInit {

  // Variables
  showData: boolean = true;
  config = {};
  tests = [];
  errorMsg;

  commonFormGroup: FormGroup;
  schemaSourceFormGroup: FormGroup;
  verifyFormGroup: FormGroup;

  // displayedColumns: string[] = ['select', 'id', 'name', 'title', 'body'];
  displayedColumns: string[] = ['select', 'position', 'name', 'weight', 'symbol'];
  dataSourceConnector = new MatTableDataSource<Test>();
  selection = new SelectionModel<Test>(true, []);

  examples = [
    { link: 'source', label: 'Source' },
    { link: 'sink', label: 'Sink' }
  ];

  private paginator: MatPaginator;
  private sort: MatSort;

  @ViewChild(MatSort) set matSort(ms: MatSort) {
    this.sort = ms;
    this.setDataSourceAttributes();
  }

  @ViewChild(MatPaginator) set matPaginator(mp: MatPaginator) {
    this.paginator = mp;
    this.setDataSourceAttributes();
  }

  setDataSourceAttributes() {
    this.dataSourceConnector.paginator = this.paginator;
    this.dataSourceConnector.sort = this.sort;

    if (this.paginator && this.sort) {
      this.applyFilter('');
    }
  }

  constructor(private _formBuilder: FormBuilder, public dialog: MatDialog, 
    public _kafkaconnectService: KafkaconnectService, private _sourceService: SourceService
  ) {}

  ngOnInit() {
    this.verifyFormGroup = this._formBuilder.group({ });
    this.getConnectors();
    this.setDataSourceAttributes();
    
    // this.dataSource.paginator = this.paginator;
    this.commonFormGroup = this._formBuilder.group({
      connectorNameCtrl: ['', Validators.required],
      selectedConnectorClassCtrl: ['', Validators.required],
      selectedTopicNameCtrl: ['', Validators.required]
    });
    this.schemaSourceFormGroup = this._formBuilder.group({
      sourceFileCtrl: ['', Validators.required],
      maxSizeCtrl: ['', Validators.required]
    });
    // this.dataSourceConnector.paginator = this.paginator;
  }

  applyFilter(filterValue: string) {
    this.dataSourceConnector.filter = filterValue.trim().toLowerCase();
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSourceConnector.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
        this.selection.clear() :
        this.dataSourceConnector.data.forEach(row => this.selection.select(row));
  }

  setShow() {
    this._kafkaconnectService.setShow();
  }

  setHidden() {
    this._kafkaconnectService.setHidden();
  }

  getShowData() {
    return this._kafkaconnectService.getShowData()
  }

  getConnectors() {
    this._sourceService.listConnectors()
    .subscribe(data => this.dataSourceConnector = new MatTableDataSource<Test>(data),
              error => this.errorMsg = error);
  }

  generateConfiguration() {
    var className = this.commonFormGroup.controls.selectedConnectorClassCtrl.value;
    var configuration = {};
    if (className == 'schemasourceconnector') {
      configuration = 
      {
        'name': 'SchemaSourceFile',
        'connector.class': this.commonFormGroup.controls.selectedConnectorClassCtrl.value,
        'topic': this.commonFormGroup.controls.selectedTopicNameCtrl.value,
        'config': {
          'tasks.max': 1,
          'file': this.schemaSourceFormGroup.controls.sourceFileCtrl.value,
          'max.size': this.schemaSourceFormGroup.controls.maxSizeCtrl.value
        }
       };
    }
    this.config = configuration;
  }
}

