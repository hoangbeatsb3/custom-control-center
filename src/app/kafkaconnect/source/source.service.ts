import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Test } from '../model/test.model';
import { tap, catchError, map } from 'rxjs/operators';
import {throwError as observableThrowError,  Observable } from 'rxjs';

const PROXY_URL = 'https://cors-anywhere.herokuapp.com/';

@Injectable()
export class SourceService {

    baseUrl: string = 'https://jsonplaceholder.typicode.com';
    test: Test[];

    constructor(private httpClient: HttpClient) {}

    // CRUD

    listConnectors(): Observable<Test[]>{
        return this.httpClient.get<Test[]>(this.baseUrl + `/posts`)
                        .pipe(tap(data => data) , catchError(this.errorHandler))
                        
    }

    createConnector(test: Test): Observable<HttpResponse<Test>> {
        let httpHeaders = new HttpHeaders({
            'Content-Type': 'application/json'
        })
        return this.httpClient.post<Test>(this.baseUrl, test,
        {
            headers: httpHeaders,
            observe: 'response'
        })
    }

    errorHandler(error: HttpErrorResponse){
        return observableThrowError(error.message || "Server Error");
    }

}
