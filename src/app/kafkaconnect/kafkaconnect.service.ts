import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class KafkaconnectService {

  constructor() { }

  showData: boolean = true;

  getShowData() {
    return this.showData;
  }

  setHidden() {
    this.showData = false;
  }

  setShow() {
    this.showData = true;
  }
}
